export default function NuevoPresupuesto() {
	return (
		<div className='contenedor-presupuesto contenedor sombra'>
			<form action='' className='formulario'>
				<div className='campo'>
					<label htmlFor='presupuesto'>Definir Presupuesto</label>
					<input
						type='text'
						id='presupuesto'
						className='nuevo-presupuesto'
						aria-label='nuevo presupuesto'
						placeholder='Agrega tu presupuesto'
					/>
					<input type='submit' value='Agregar Presupuesto' aria-label='Agregar Presupuesto' />
				</div>
			</form>
		</div>
	);
}
